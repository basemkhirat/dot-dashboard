<?php

namespace Dot\Dashboard\Controllers;

use Dot\Platform\Controller;
use Lang;
use Request;
use Session;

/**
 * Class DashboardController
 * @package Dot\Dashboard\Controllers
 */
class DashboardController extends Controller
{

    /**
     * View payload
     * @var array
     */
    public $data = [];

    /**
     * Show the dashboard
     * @return mixed
     */
    public function index()
    {
        return view("dashboard::show", $this->data);
    }

}
